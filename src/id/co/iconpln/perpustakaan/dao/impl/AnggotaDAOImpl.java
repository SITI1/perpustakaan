/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao.impl;

import id.co.iconpln.perpustakaan.dao.AnggotaDAO;
import id.co.iconpln.perpustakaan.entity.Anggota;
import id.co.iconpln.perpustakaan.util.DBConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import jdk.internal.instrumentation.Logger;


public class AnggotaDAOImpl implements AnggotaDAO {
    
private final Connection connection = DBConnection.getInstance().getConnetion()
        
    @Override
    public Anggota save(Anggota param) {
        
        String sql = "INSERT INTO Anggota ("
                +"no,"
                +"nama)"
                +"values(?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, param.getNo());
            preparedStatement.setString(2, param.getNama());
            preparedStatement.executeUpdate();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs !=null && rs.next()) {
                param.setId(rs.getInt(1));
            } 
        } catch (SQLException ex) {
           logger.getLogger(BukuDAOImpl.class.getName()).Log(Level.SERVE ,null, ex);
        }
        return param;
    }

    @Override
    public Anggota upate(Anggota param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Anggota delete(Anggota delete) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Anggota findById(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Anggota> find(Anggota param) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
