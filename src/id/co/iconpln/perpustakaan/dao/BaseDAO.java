/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.dao;

import java.util.List;

/**
 *
 * @author SITI
 * @param <T>
 */
public interface BaseDAO<T> {

    T save(T param);

    T upate(T param);

    T delete(T delete);

    T findById(int id);

    List<T> find(T param);
}
