/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.service;

import id.co.iconpln.perpustakaan.dao.BukuDAO;
import id.co.iconpln.perpustakaan.dao.impl.BukuDAOImpl;
import id.co.iconpln.perpustakaan.entity.Buku;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author SITI
 */
public class ServiceDAO {

    private final BukuDAO bukuDAO = new BukuDAOImpl();

    public List<Buku> findBukus(Buku param) {
        return bukuDAO.find(param);
    }

    public Buku save(Buku param) {
        return bukuDAO.save(param);
    }

}
