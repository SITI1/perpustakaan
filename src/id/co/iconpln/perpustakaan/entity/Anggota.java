/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.entity;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author SITI
 */
public class Anggota {

    private int id;
    private String no; //no anggota
    private String nama;
    private Set<Telepon> telepons = new HashSet<>(); //tidak menggunakan List karena nomor telp yidak boleh sama, list no boleh sama

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Set<Telepon> getTelepons() {
        return telepons;
    }

    public void setTelepons(Set<Telepon> telepons) {
        this.telepons = telepons;
    }

    @Override
    public String toString() {
        return "Anggota{" + "id=" + id + ", no=" + no + ", nama=" + nama + ", telepons=" + telepons + '}';
    }

}
