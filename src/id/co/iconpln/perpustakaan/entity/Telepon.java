/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.co.iconpln.perpustakaan.entity;

/**
 *
 * @author SITI
 */
public class Telepon {

    private int id;
    private String no; //no telepon

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    @Override
    public String toString() {
        return "Telepon{" + "id=" + id + ", no=" + no + '}';
    }

}
